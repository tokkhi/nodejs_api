// import
const bodyParser = require('body-parser')
const express = require('express');
const db = require('../util/config');
const { response } = require('express');
// define variable
const sequelize = db.sequelize;
const test = db.test;
const route = express.Router();
const jsonParser = bodyParser.json()

// get by id
route.get('/:id', async (req, res, next) => {
  const id = req.params.id;
  let tests = {};
  if (id) {
    data = await test.findById(id);
  }
  response.data = data
  response.statusCode = 200
  res.json(response);
});

route.get('/', async (req, res, next) => {
  const list = await test.findAll();
  response.data = list
  response.statusCode = 200
  res.json(response);
});

//create
route.post('/create', async (req, res, next) => {
  console.log('body::==', req.body);
  console.log('params::==', req.params);
  const data = req.body;
  let newData = null;
  if (data) {
    try{
       sequelize.transaction().then(function(t) {
         test.create(data, {
            transaction: t
        }).then(function() {
            t.commit();
            
        }).catch(function(error) {
            console.log(error);
            t.rollback();
        });
      })
      response.statusCode = 200
    }catch(error){
      response.statusCode = 500
    }
    
  }
  res.json(response);
});

//update
route.put('/update/:id', async (req, res, next) => {
  console.log('body::==', req.body);
  console.log('params::==', req.params);
  const data = req.body;
  const id = req.params.id;
  let updateData = null;
  if (data && id) {
    test.update(
      data
    , {
      where: { id: id },
    })
    .then(function (result) {
      console.log(result);   
      
    });
  }
  res.json(updateData);
});

//delete
route.delete('/delete/:id', async (req, res, next) => {
  console.log('body::==', req.body);
  console.log('params::==', req.params);
  const id = req.params.id;
  let dataDel = null;
  if (id) {
    const data = await test.findById(id);
    if (data) {
      dataDel = await data.destroy();
    }
  }
  res.json(dataDel);
});

module.exports = route;