module.exports = (sequelize, Sequelize) => {
    const test = sequelize.define(
      'test',
      {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true
      },
        data: {
          type: Sequelize.STRING,
          field: 'data'
        }
      },
      {
        timestamps: false,
        freezeTableName: true
      }
    );
    return test;
  };